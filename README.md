# TMUX

TMUX program installation notes.

# Table of Contents

1. [Installation](#1-installation)
2. [How to use TMUX](#2-how-to-use-tmux)

# 1) Installation

I will cover the installation of TMUX on the following Linux distributions:

* **Arch Linux**

  Just run one of the following commands:

  ```zsh
  $ sudo pacman -S tmux --noconfirm
  ```

* **Ubuntu or Debian**

  Just run the following commands:
  ```zsh
  $ sudo apt-get update
  $ sudo apt-get install tmux
  ```

* **CentOS**

  Just run the following commands:
  ```zsh
  $ sudo yum -y install tmux
  ```

Once TMUX is installed, in order to create your custom configuration, you have **three** options:
1. Use the current (default) settings as a starting point:
     ```zsh
     $tmux show -g | cat > ~/.tmux.conf
     ```
2. Create your empty **tmux.conf** configuration file from scratch:
     ```zsh
     $touch ~/.tmux.conf
     ```
3. Download my own **tmux.conf** and modify it according to your needs:
     ```zsh
     $wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/.tmux.conf ~/.tmux.conf
     ```

Next, create the following "tmux" folder inside **~.config/** directory:
  ```zsh
  $cd ~
  $cd .config/
  $mkdir tmux
  ```
And as a staring point you can download the following scripts **battery.sh**, **docker.sh** and **ram.sh** and include them inside the repository's **config** folder.
  ```zsh
  $wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/config/battery.sh ~/.config/tmux/battery.sh
  $wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/config/docker.sh ~/.config/tmux/docker.sh
  $wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/config/ram.sh ~/.config/tmux/ram.sh
  $chmod u+x docker.sh ram .sh
  ```

## 1-1) TMUX Plugins
Finally, if you plan to use tmux plugins, you must install the "TPM" (TMUX Plugin Manager) as follows:
```zsh
$git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

### 1-1-1) Installing plugins

  1. Add new plugin to **~/.tmux.conf** with set -g @plugin '...'
  2. Press **prefix + I** (capital i, as in Install) to fetch the plugin.

You're good to go! The plugin was cloned to **~/.tmux/plugins/** dir and sourced.

### 1-1-2) Uninstalling plugins

  1. Remove (or comment out) plugin from the list.
  2. Press **prefix + alt + u** (lowercase u as in uninstall) to remove the plugin.

All the plugins are installed to **~/.tmux/plugins/** so alternatively you can find plugin directory there and remove it.

### 1-1-3) Key bindings

* **prefix + I**

  * Installs new plugins from GitHub or any other git repository
  * Refreshes TMUX environment

* **prefix + U**

  * updates plugin(s)

* **prefix + alt + u**

  * remove/uninstall plugins not on the plugin list

* **More plugins**

  For more plugins, check: https://github.com/tmux-plugins

And that's all. Enjoy it!

# 2) How to use TMUX

After the installation, you can start using tmux. The following tips will give you heads on how to use tmux.

* Start tmux:
```zsh 
$ tmux
```

* Detach from tmux:
To detach from active tmux session, type control+b followed by d
```zsh 
 $ ctrl-a d
```

* Restore tmux session:
 
To attach to detached tmux session, use:
```zsh 
 $ tmux attach 
```

* Create new tmux session with name:

Use the new sub-command with -s session to give it a name.
```zsh 
 $ tmux new -s test
```

* Attach to named detached tmux session:

If you detach tmux session, you can always re-attach using a command with -t option
```zsh 
 $ tmux a -t test
```

* Display tmux sessions:

Show all active tmux sessions using the command.
```zsh 
 $ tmux ls
``` 

* Rename session:

The following command is used to rename an active session.
```zsh
 $ Ctrl-a $
``` 

Provide session name and press enter key.

* Switch session:

Use this key combination to switch session.
```zsh
 $ Ctrl-a s
``` 

* Show tmux help screen (Q to quit):

Tmux help screen can be checked with:
```zsh
 $ Ctrl-a ?
``` 

## 2-1) Windows Management

* Create a new window:
```zsh
 $ Ctrl-a c
```

* Destroy tmux window:
```zsh
 $ Ctrl-a x
```

* Switch between windows:
```zsh
 $ Ctrl-a [0-9] or Ctrl-a Arrows
```

* Split windows horizontally:
```zsh
 $ Ctrl-a h
```

* Split windows vertically:
```zsh
 $ Ctrl-a v
```

These are the most common tmux operations and commands you’ll need when working with tmux sessions. If you know of tmux commands not here, you can share on the comments section.
