#!/bin/bash
# This needs to be in the home folder and needs
# to be chmod +x so it runs right.

BAT_0_PATH=/sys/class/power_supply/BAT0
STATUS_0=$BAT_0_PATH/status
BAT_0_FULL=$BAT_0_PATH/energy_full
BAT_0_NOW=$BAT_0_PATH/energy_now

BAT_1_PATH=/sys/class/power_supply/BAT1
STATUS_1=$BAT_1_PATH/status
BAT_1_FULL=$BAT_1_PATH/energy_full
BAT_1_NOW=$BAT_1_PATH/energy_now

bf_0=$(cat $BAT_0_FULL)
bf_1=$(cat $BAT_1_FULL)

bn_0=$(cat $BAT_0_NOW)
bn_1=$(cat $BAT_1_NOW)

BAT_0=`echo "100 * $bn_0 / $bf_0" | bc`
BAT_1=`echo "100 * $bn_1 / $bf_1" | bc`

#echo "Batt[int|ext]:" $BAT_0% "|" $BAT_1%
echo $BAT_0% "|" $BAT_1%