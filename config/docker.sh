#!/usr/bin/env bash

DOCKER_CONTAINERS_RUNNING=$(expr "$(docker ps | wc -l)" - 1)
DOCKER_ICON="\uf21a"

echo -e "$DOCKER_ICON : $DOCKER_CONTAINERS_RUNNING"