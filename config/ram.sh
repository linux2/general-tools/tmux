#!/usr/bin/env bash

MEM_FREE=$(cat /proc/meminfo | grep MemAvailable | awk '{printf "%.2f", $2/1000000}')

echo -e "$MEM_FREE"